extern crate clap;
pub use clap::Parser;
use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead, BufReader};

type CatrResult<T> = Result<T, Box<dyn Error>>;

#[derive(Parser)]
#[command(author, version, about, long_about = None)] // Read from `Cargo.toml`
pub struct Cli {
    /// Input files
    pub files: Option<Vec<String>>,

    /// Number all output lines
    #[arg(short = 'n', long = "number", group = "number")]
    pub number_lines: bool,
    /// Number all non-blank output lines
    #[arg(short = 'b', long = "number-nonblank", group = "number")]
    pub number_nonblank_lines: bool,
}

pub fn run(cli: Cli) -> CatrResult<()> {
    let files = cli.files.unwrap_or(vec!["-".to_string()]);
    for filename in files {
        match open(&filename) {
            Err(e) => eprintln!("{}: {}", filename, e),
            Ok(file) => {
                let mut last_non_blankline_count = 0;
                for (line_num, line_result) in file.lines().enumerate() {
                    let line = line_result?;
                    /* if -n is given, we can just use the index from enumerate() for our
                     * line count
                     */
                    if cli.number_lines {
                        println!("{:6}\t{}", line_num + 1, line);
                    }
                    else if cli.number_nonblank_lines {
                        /* increment last_non_blankline_count only for non-empty lines */
                        if !line.is_empty() {
                            last_non_blankline_count += 1;
                            println!("{:6}\t{}", last_non_blankline_count, line);
                        }
                        /* print just a newline for empty lines */
                        else {
                            println!();
                        }
                    }
                    else {
                        println!("{}", line);
                    }
                }
            }
        }
    }
    Ok(())
}

fn open(filename: &str) -> CatrResult<Box<dyn BufRead>> {
    match filename {
        "-" => Ok(Box::new(BufReader::new(io::stdin()))),
        _ => Ok(Box::new(BufReader::new(File::open(filename)?))),
    }
}
