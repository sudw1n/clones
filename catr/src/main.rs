use catr::{Parser, Cli};

fn main() {
    let cli = Cli::parse();
    if let Err(error) = catr::run(cli) {
        eprintln!("{}", error);
        std::process::exit(1);
    }
}
