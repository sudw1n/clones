use regex::Regex;
use std::error::Error;
use walkdir::WalkDir;

pub type FindrResult<T> = Result<T, Box<dyn Error>>;

#[derive(Clone, Ord, PartialOrd, Debug, Eq, PartialEq)]
pub enum EntryType {
    Dir,
    File,
    Link,
}

impl Default for EntryType {
    fn default() -> Self {
        EntryType::File
    }
}

#[derive(Debug)]
pub struct Config {
    pub names: Vec<Regex>,
    pub entry_types: Vec<EntryType>,
    pub paths: Vec<String>,
}

impl Config {
    pub fn and_then<F>(self, after: F) -> FindrResult<()>
    where
        F: Fn(Config) -> FindrResult<()>,
    {
        after(self)?;
        Ok(())
    }
}

pub fn run(config: Config) -> FindrResult<()> {
    // check for `--type` options
    let type_filter = |entry: &walkdir::DirEntry| -> bool {
        config.entry_types.is_empty()
            || config
                .entry_types
                .iter()
                .any(|entry_type| match entry_type {
                    // use file_type() instead of path()
                    EntryType::Link => entry.file_type().is_symlink(),
                    EntryType::Dir => entry.file_type().is_dir(),
                    EntryType::File => entry.file_type().is_file(),
                })
    };

    // check for `--name` options
    let name_filter = |entry: &walkdir::DirEntry| -> bool {
        config.names.is_empty()
            || config
                .names
                .iter()
                .any(|re| re.is_match(&entry.file_name().to_string_lossy()))
    };

    for path in &config.paths {
        let entries = WalkDir::new(path)
            .into_iter()
            .filter_map(|e| match e {
                Err(e) => {
                    eprintln!("{}", e);
                    None
                }
                Ok(entry) => Some(entry),
            })
            .filter(type_filter)
            .filter(name_filter)
            .map(|entry| entry.path().display().to_string())
            .collect::<Vec<_>>();

        println!("{}", entries.join("\n"));
    }

    Ok(())
}
