use std::process;

use clap::{arg, command, ArgAction, Command};
use findr::{Config, EntryType, FindrResult};
use regex::Regex;

fn main() {
    if let Err(e) = get_args().and_then(findr::run) {
        eprintln!("{}", e);
        process::exit(1);
    }
}

fn get_args() -> FindrResult<Config> {
    let matches = cli().get_matches();
    let mut entry_types: Vec<EntryType> = matches
        .get_many::<String>("type")
        .unwrap_or_default()
        .map(|entry_type| match entry_type.as_str() {
            "d" => EntryType::Dir,
            "f" => EntryType::File,
            "l" => EntryType::Link,
            _ => unreachable!("Invalid type"),
        })
        .collect::<Vec<_>>();
    /* remove duplicate entries */
    entry_types.sort();
    entry_types.dedup();

    let names: Vec<Regex> = matches
        .get_many::<String>("name")
        .unwrap_or_default()
        .map(|name| {
            Regex::new(&name).map_err(|_| format!("Invalid regex given in --name \"{}\"", &name))
        })
        .collect::<Result<Vec<_>, _>>()?;

    let paths: Vec<String> = matches
        .get_many::<String>("paths")
        .unwrap_or_default()
        .map(|path| path.clone())
        .collect();

    Ok(Config {
        names,
        entry_types,
        paths,
    })
}

fn cli() -> Command {
    command!()
        .arg(
            arg!(paths: <PATHS> "Search paths")
                .required(false)
                .default_value(".")
                .action(ArgAction::Append),
        )
        .arg(
            arg!(-t --type <filetype> "Filter search by type")
                .required(false)
                .value_parser(["d", "f", "l"])
                .action(ArgAction::Append),
        )
        .arg(
            arg!(-n --name <pattern> "Pattern (regex) to match with")
                .required(false)
                .action(ArgAction::Append),
        )
}
