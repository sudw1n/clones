extern crate clap;

use clap::{Arg, command, ArgAction};

fn main() {
    let matches = command!()
        .arg(
            Arg::new("text")
            .action(ArgAction::Append)
            .required(true)
            .help("Input text")
            )
        .arg(
            Arg::new("omit_newline")
            .short('n')
            .action(ArgAction::SetTrue)
            .help("Omit newline in output")
            )
        .get_matches();
    let text: Vec<&str> = matches.get_many::<String>("text").unwrap().map(|s| s.as_str()).collect();
    let ending = if matches.get_flag("omit_newline") { "" } else { "\n" };
    print!("{}{}", text.join(" "), ending);
}
