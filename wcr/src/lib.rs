use clap::Parser;
use std::{error::Error, io::{self, BufRead, BufReader}};
use std::fs::File;

// "-" means stdout
const _STDOUT_FILENAME: &str = "-";

type WcrResult<T> = Result<T, Box<dyn Error>>;

/// The struct representing options and input files for `wcr`
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = "Prints the line, word and byte count in the given input files")]
pub struct Config {
    /// input files
    #[arg(default_values_t = vec![String::from("-")])]
    files: Vec<String>,
    /// print the number of lines
    #[arg(short = 'l', long)]
    lines: bool,
    /// print the number of words
    #[arg(short = 'w', long)]
    words: bool,
    /// print the number of bytes
    #[arg(short = 'c', long, group = "bytes_chars")]
    bytes: bool,
    /// print the number of characters
    #[arg(short = 'm', long, group = "bytes_chars")]
    chars: bool,
}

/// Struct representing the number of bytes, lines, words and characters in the file
#[derive(PartialEq, Debug)]
pub struct FileInfo {
    num_lines: usize,
    num_words: usize,
    num_bytes: usize,
    num_chars: usize,
}

/// Main driver code for `wcr`
/// Takes the instance of the Config struct, the values should be initialised by `clap` with the
/// values given at the commandline
pub fn run(config: Config) -> WcrResult<()>
{
    let mut total_lines = 0;
    let mut total_words = 0;
    let mut total_bytes = 0;
    let mut total_chars = 0;

    let mut lines = config.lines;
    let mut words = config.words;
    let mut bytes = config.bytes;
    let chars = config.chars;

    if [words, bytes, chars, lines].iter().all(|v| v == &false) {
        lines = true;
        words = true;
        bytes = true;
    }

    for filename in &config.files {
        match open(filename) {
            Err(err) => eprintln!("{}: {}", filename, err),
            Ok(file) => {
                if let Ok(counts) = count(file) {
                    println!(
                        "{}{}{}{}{}",
                         format_field(counts.num_lines, lines),
                         format_field(counts.num_words, words),
                         format_field(counts.num_bytes, bytes),
                         format_field(counts.num_chars, chars),
                         if filename == "-" {
                             "".to_string()
                         }
                         else {
                             format!(" {}", filename)
                         }
                     );
                    total_lines += counts.num_lines;
                    total_words += counts.num_words;
                    total_bytes += counts.num_bytes;
                    total_chars += counts.num_chars;
                }
            }
        }
    }
    if config.files.len() > 1 {
        println!("{}{}{}{} total",
                 format_field(total_lines, lines),
                 format_field(total_words, words),
                 format_field(total_bytes, bytes),
                 format_field(total_chars, chars),
             );
    }
    Ok(())
}

fn format_field(value: usize, show: bool) -> String {
    if show {
        format!("{:>8}", value)
    }
    else {
        "".to_string()
    }
}

fn count(mut file: impl BufRead) -> WcrResult<FileInfo> {
    let mut num_lines = 0_usize;
    let mut num_words = 0_usize;
    let mut num_bytes = 0_usize;
    let mut num_chars = 0_usize;

    let mut line = String::new();
    loop {
        let line_bytes = file.read_line(&mut line)?;
        if line_bytes == 0 {
            break;
        }
        num_lines += 1;
        num_words += line.split_whitespace().count();
        num_bytes += line_bytes;
        num_chars += line.chars().count();

        line.clear();
    }

    Ok(
        FileInfo {
            num_lines,
            num_words,
            num_bytes,
            num_chars
        }
    )
}

/// Opens a given file and returns a BufReader from it
/// If the given filename is "-", then it opens `io::stdin()`
fn open(filename: &str) -> WcrResult<Box<dyn BufRead>>
{
    match filename {
        "-" => Ok(Box::new(BufReader::new(io::stdin()))),
        _ => Ok(Box::new(BufReader::new(File::open(filename)?))),
    }
}

#[cfg(test)]
mod tests {
    use super::{count, FileInfo, format_field};
    use std::io::Cursor;

    #[test]
    fn test_count() {
        let text = "After life's fitful fever, he sleeps well.\r\n";
        let info = count(Cursor::new(text));
        assert!(info.is_ok());
        let expected = FileInfo {
            num_lines: 1,
            num_words: 7,
            num_bytes: 44,
            num_chars: 44,
        };
        assert_eq!(info.unwrap(), expected);
    }

    #[test]
    fn test_format_field() {
        assert_eq!(format_field(1, false), "");
        assert_eq!(format_field(3, true), "       3");
        assert_eq!(format_field(10, true), "      10");
    }
}
