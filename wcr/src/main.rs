use clap::Parser;
use wcr::{Config};

fn main() {
    let config = Config::parse();
    if let Err(err) = wcr::run(config) {
        eprintln!("{}", err);
    }
}
