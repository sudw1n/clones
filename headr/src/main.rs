use clap::Parser;
use headr::{Config};

fn main() {
    let config = Config::parse();
    if let Err(err) = headr::run(config) {
        eprintln!("{}", err);
    }
}
