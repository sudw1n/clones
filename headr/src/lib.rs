use clap::Parser;
use std::{error::Error, io::{self, BufRead, BufReader}};
use std::fs::File;

type HeadrResult<T> = Result<T, Box<dyn Error>>;

/// The struct representing options and input files for `headr`
#[derive(Parser, Debug)]
#[command(author, version, about)]
pub struct Config {
    /// Input files
    files: Option<Vec<String>>,
    /// Number of lines to print
    #[arg(short = 'n', long, default_value_t = 10, group = "limit")]
    lines: usize,
    /// Number of characters to print
    #[arg(short = 'c', long, group = "limit")]
    bytes: Option<usize>
}

/// Main driver code for `headr`
/// Takes the instance of the Config struct, the values should be initialised by `clap` with the
/// values given at the commandline
pub fn run(config: Config) -> HeadrResult<()>
{
    let files = config.files.unwrap_or(vec!["-".to_string()]);
    let files_len = files.len();
    for (file_num, filename) in files.iter().enumerate() {
        match open(&filename) {
            Err(err) => eprintln!("{}: {}", filename, err),
            Ok(file) => {
                if files_len > 1 {
                    // Print a new line if this isn't the first file
                    println!("{}==> {filename} <==",
                             if file_num > 0 { "\n" } else { "" }
                             );
                }
                if let Some(bytes) = config.bytes {
                    write_n_bytes(file, bytes)?;
                }
                else {
                    write_n_lines(file, config.lines)?;
                }
            },
        }
    }
    Ok(())
}

/// Writes at most `n` bytes from the given `input`
/// `input` must implement the BufRead trait
fn write_n_bytes<R>(input: R, n: usize) -> HeadrResult<()>
where R: BufRead
{
    // Read the first `n` bytes from the `input` reader
    let mut reader = input.take(n.try_into().expect("given number of bytes should be convertible from a usize into u64"));

    // write those bytes to stdout
    let mut stdout = io::stdout().lock();
    io::copy(&mut reader, &mut stdout)?;

    Ok(())
}

/// Writes at most `n` lines from the given `input`
/// `input` must implement the BufRead trait
fn write_n_lines<R>(mut input: R, n: usize) -> HeadrResult<()>
where R: BufRead
{
    let mut line = String::new();
    for _ in 0..n {
        let bytes = input.read_line(&mut line)?;
        if bytes == 0 {
            break;
        }
        print!("{}", line);
        line.clear();
    }
    Ok(())
}

/// Opens a given file and returns a BufReader from it
/// If the given filename is "-", then it opens `io::stdin()`
fn open(filename: &str) -> HeadrResult<Box<dyn BufRead>>
{
    match filename {
        "-" => Ok(Box::new(BufReader::new(io::stdin()))),
        _ => Ok(Box::new(BufReader::new(File::open(filename)?))),
    }
}
