//! Contains command parsers and logic.

use std::io::{self, BufRead};
use std::u8;
use std::{ops::Range, process};

use clap::{arg, command, ArgAction, ArgGroup, Command};
use csv::{ReaderBuilder, WriterBuilder};

use crate::{extract_bytes, extract_chars, extract_fields, open, parse_pos};

pub type SelectionRange = Vec<Range<usize>>;

#[derive(Clone, Debug)]
pub enum Selection {
    Fields(SelectionRange),
    Bytes(SelectionRange),
    Chars(SelectionRange),
}

#[derive(Debug)]
pub struct Config {
    pub files: Vec<String>,
    pub delimiter: u8,
    pub selection: Selection,
}

impl Config {
    pub fn parse() -> Config {
        let args = get_args().get_matches();

        // Extract the selection based on which option was used.
        // Print the error message and exit the program if the given range is invalid
        let pos_error = |err| {
            eprintln!("{}", err);
            process::exit(2);
        };
        let selection = if let Some(byte_range) = args.get_one::<String>("bytes") {
            Selection::Bytes(parse_pos(&byte_range).unwrap_or_else(pos_error))
        } else if let Some(char_range) = args.get_one::<String>("chars") {
            Selection::Chars(parse_pos(&char_range).unwrap_or_else(pos_error))
        } else if let Some(field_range) = args.get_one::<String>("fields") {
            Selection::Fields(parse_pos(&field_range).unwrap_or_else(pos_error))
        } else {
            unreachable!("at least one selection range should have been given since it's required");
        };

        let delimiter_str = args
            .get_one::<String>("delimiter")
            .expect("delimiter argument should not be None since we have provided a default");
        let delimiter_raw = delimiter_str.as_bytes();
        if delimiter_raw.len() != 1 {
            eprintln!("--delim \"{}\" must be a single byte", delimiter_str);
            process::exit(3);
        }
        let delimiter = *delimiter_raw.first().expect("the delimiter_raw vector should have exactly one byte as we have checked for it before");

        let files = args
            .get_many::<String>("files")
            .expect("files argument should not be None since we have provided a default")
            .map(|val| val.clone())
            .collect();

        Config {
            files,
            delimiter,
            selection,
        }
    }
    pub fn run(self) -> crate::Result<()> {
        for filename in &self.files {
            match open(filename) {
                Err(err) => eprintln!("{}: {}", filename, err),
                Ok(file) => match &self.selection {
                    Selection::Fields(field_pos) => {
                        let mut reader = ReaderBuilder::new()
                            .delimiter(self.delimiter)
                            .has_headers(false)
                            .from_reader(file);
                        let mut writer = WriterBuilder::new()
                            .delimiter(self.delimiter)
                            .from_writer(io::stdout());

                        for record in reader.records() {
                            let record = record?;
                            writer.write_record(extract_fields(&record, field_pos))?;
                        }
                    }
                    Selection::Bytes(byte_pos) => {
                        for line in file.lines() {
                            println!("{}", extract_bytes(&line?, byte_pos));
                        }
                    }
                    Selection::Chars(char_pos) => {
                        for line in file.lines() {
                            println!("{}", extract_chars(&line?, char_pos));
                        }
                    }
                },
            }
        }
        Ok(())
    }
}

fn get_args() -> Command {
    command!()
        .arg(arg!(-b --bytes <BYTES> "Selected bytes"))
        .arg(arg!(-c --chars <CHARS> "Selected characters"))
        .arg(arg!(-f --fields <FIELDS>   "Selected fields"))
        .group(
            ArgGroup::new("selection")
                .required(true)
                .args(["bytes", "chars", "fields"]),
        )
        .arg(arg!(delimiter: -d --delim <DELIMITER> "Field delimiter").default_value("\t"))
        .arg(
            arg!(files: <FILE>... "Input file(s)")
                .default_value("-")
                .action(ArgAction::Append),
        )
}
