use std::process;

use cutr::Config;

fn main() {
    let cli = Config::parse();
    if let Err(e) = cli.run() {
        eprintln!("{}", e);
        process::exit(1);
    }
}
