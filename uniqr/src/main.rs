use clap::Parser;
use std::{error::Error, io::{BufRead, BufReader, self, Write}};
use std::fs::File;

type UniqrResult<T> = Result<T, Box<dyn Error>>;

#[derive(Parser, Debug)]
#[command(author, about, version, long_about = "Prints the unique occurence of each line from given input, with an optional count.")]
struct Config {
    /// Input file name
    #[arg(default_value_t = String::from("-"))]
    in_file: String,
    /// Output file
    out_file: Option<String>,
    /// Show counts
    #[arg(short, long, default_value_t = false)]
    count: bool,
}

fn main() {
    let config = Config::parse();
    if let Err(err) = run(config) {
        eprintln!("{}", err);
        std::process::exit(1);
    }
}

fn run(config: Config) -> UniqrResult<()> {
    let mut in_file = open_input_file(&config.in_file)
        .map_err(|e| format!("{}: {}", config.in_file, e))?;
    let mut out_file = open_output_file(&config.out_file)?;

    let mut line = String::new();

    // we track the count of `previous`
    let mut previous = String::new();
    let mut count = 0_u64;

    // writes the given count and line of text to the given output file
    let mut write_output = |count: u64, text: &str| -> UniqrResult<()> {
        if count > 0 {
            if config.count {
                write!(out_file, "{:>7} {}", count, text)?;
            }
            else {
                write!(out_file, "{}", text)?;
            }
        }
        Ok(())
    };

    loop {
        let bytes = in_file.read_line(&mut line)?;
        if bytes == 0 {
            break;
        }

        // if the new line read isn't the same as the one we're counting
        if line.trim_end() != previous.trim_end() {
            // at the first iteration, previous will be empty and count will be 0 so add a check to
            // not print anything if count hasn't increased
            if count > 0 {
                // print the total count
                write_output(count, &previous)?;
            }
            // update the line to be counted
            previous = line.clone();
            // reset the count
            count = 0;
        }
        // otherwise update the count
        count += 1;
        line.clear();
    }

    // at the last iteration of the loop, we'll have the count of the last line read,
    // but that count won't be printed
    write_output(count, &previous)?;
    Ok(())
}

fn open_input_file(filename: &str) -> UniqrResult<Box<dyn BufRead>> {
    match filename {
        "-" => Ok( Box::new( BufReader::new(io::stdin()) ) ),
        _ => Ok( Box::new( BufReader::new(File::open(filename)?) ) ),
    }
}

fn open_output_file(out_file: &Option<String>) -> UniqrResult<Box<dyn Write>> {
    match out_file {
        Some(out_filename) => Ok(Box::new(File::create(out_filename)?)),
        None => Ok(Box::new(io::stdout())),
    }
}
